class Entry < ActiveRecord::Base
  #Validators for entries
  before_save :calculate_duration
  validates :title, :start_time, :end_time, presence: true
  validate :start_time_before_end_time

  #Calulate duration in min., round up to 15 min.
  def calculate_duration
    minutes = ((end_time - start_time) / 60.0).round
    mod = minutes % 15
    self.duration = mod == 0 ? minutes : (minutes + 15 - mod)
  end

  #Custom vailidator, make sure end_time in not less than start_time
  def start_time_before_end_time
    if end_time < start_time
      errors.add(:end_time, "má ekki vera á undan Byrjar")
    end
  end
end
