class EntriesController < ApplicationController


  #Overview of entries
  def index
    #Get date filters
    @from = params[:from_submit].try(:to_date) || 1.month.ago.to_date
    @to = params[:to_submit].try(:to_date) || Date.today

    #Get entries
    @entries = Entry.where(start_time: @from.beginning_of_day..@to.end_of_day)
  end

  #New entry
  def new
    @entry = Entry.new
  end

  #Create entry
  def create
    @entry = Entry.new(entry_params)
    if @entry.save
      redirect_to entries_path
    else
      render 'new'
    end
  end

  #Show entry
  def show
    @entry = Entry.find(params[:id])
  end

  #Delete entry
  def destroy
    Entry.find(params[:id]).destroy
    redirect_to entries_path
  end

  #Edit entry
  def edit
    @entry = Entry.find(params[:id])
  end

  #Update entry
  def update
    @entry = Entry.find(params[:id])
    if @entry.update_attributes(entry_params)
      redirect_to entries_path
    else
      render 'edit'
    end
  end

  private
    #Parameters for entry
    def entry_params
      params.require(:entry).permit(:title, :body, :start_time, :end_time)
    end
end