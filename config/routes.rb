Verkdagbok::Application.routes.draw do
  resources :entries, path: "faerslur"
  root to: "entries#index"
end
