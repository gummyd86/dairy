class CreateEntries < ActiveRecord::Migration
  def change
    create_table :entries do |t|
      t.string :title
      t.text :body
      t.datetime :start
      t.datetime :end
      t.integer :duration

      t.timestamps
    end
  end
end
