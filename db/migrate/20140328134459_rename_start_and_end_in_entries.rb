class RenameStartAndEndInEntries < ActiveRecord::Migration
  def change
    rename_column :entries, :start, :start_time
    rename_column :entries, :end, :end_time
  end
end
